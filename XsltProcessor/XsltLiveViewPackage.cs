﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.TextTemplating.VSHost;
using XsltLiveView.Generators;
using Task = System.Threading.Tasks.Task;

namespace XsltLiveView
{
    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the
    /// IVsPackage interface and uses the registration attributes defined in the framework to
    /// register itself and its components with the shell. These attributes tell the pkgdef creation
    /// utility what data to put into .pkgdef file.
    /// </para>
    /// <para>
    /// To get loaded into VS, the package must be referred by &lt;Asset Type="Microsoft.VisualStudio.VsPackage" ...&gt; in .vsixmanifest file.
    /// </para>
    /// </remarks>
    [PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
    [Guid(XsltLiveViewPackage.PackageGuidString)]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    //[ProvideToolWindow(typeof(SettingsWindow), Style =VsDockStyle.Tabbed, Window = "3ae79031-e1bc-11d0-8f78-00a0c9110057")]
    [ProvideCodeGenerator(typeof(XsltOutputGenerator), XsltOutputGenerator.Name, XsltOutputGenerator.Description, true)]
    [ProvideUIContextRule("958B75BC-FF5C-4DD5-A45E-123D7ABA832B", // Must match the GUID in the .vsct file
        name: "UI Context",
        expression: "xslt", // This will make the button only show on .xslt files
        termNames: new[] { "xslt"},
        termValues: new[] { "HierSingleSelectionName:.xslt$"})]
    public sealed class XsltLiveViewPackage : AsyncPackage
    {
        /// <summary>
        /// XsltLiveViewPackage GUID string.
        /// </summary>
        public const string PackageGuidString = "1ce902e4-2b81-4592-8521-00398e59bb67";

        #region Package Members

        /// <summary>
        /// Initialization of the package; this method is called right after the package is sited, so this is the place
        /// where you can put all the initialization code that rely on services provided by VisualStudio.
        /// </summary>
        /// <param name="cancellationToken">A cancellation token to monitor for initialization cancellation, which can occur when VS is shutting down.</param>
        /// <param name="progress">A provider for progress updates.</param>
        /// <returns>A task representing the async work of package initialization, or an already completed task if there is none. Do not return null from this method.</returns>
        protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
        {
            // When initialized asynchronously, the current thread may be a background thread at this point.
            // Do any initialization that requires the UI thread after switching to the UI thread.
            await this.JoinableTaskFactory.SwitchToMainThreadAsync(cancellationToken);
            //await EnableMenuCommand.InitializeAsync(this);
            //await SettingsWindowCommand.InitializeAsync(this);
            await XsltLiveView.Commands.ApplyXsltLiveView.InitializeAsync(this);
        }

        #endregion
    }
}
