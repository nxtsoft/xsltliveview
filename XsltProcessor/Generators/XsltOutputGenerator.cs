﻿using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TextTemplating.VSHost;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using System.Xml.Xsl;

namespace XsltLiveView.Generators
{
    [Guid("FA864BA7-2370-4843-892C-AFE90FAB638E")]
    public class XsltOutputGenerator : BaseCodeGeneratorWithSite
    {
        public const string Name = nameof(XsltOutputGenerator);
        public const string Description = "Generates output XML file based on input XML and related XSLT";

        public override string GetDefaultExtension()
        {            
            return "_output.xml"; 
        }        

        protected override byte[] GenerateCode(string inputFileName, string inputFileContent)
        {
            Microsoft.VisualStudio.Shell.ThreadHelper.ThrowIfNotOnUIThread();
            try
            {
                string xmlInputFileName = GetXmlInputFile(inputFileName);
                if (String.IsNullOrEmpty(xmlInputFileName))
                {
                    return new byte[] { };
                }

                XmlDocument xmlFile = new XmlDocument();
                xmlFile.LoadXml(File.ReadAllText(xmlInputFileName));

                XslCompiledTransform transform = new XslCompiledTransform();
                transform.Load(inputFileName);

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.Encoding = Encoding.UTF8;
                settings.IndentChars = ("\t");

                StringBuilder builder = new StringBuilder();

                using (XmlWriter writer = XmlWriter.Create(builder, settings))
                {
                    transform.Transform(xmlFile, writer);
                }

                return Encoding.UTF8.GetBytes(builder.ToString());
            } 
            catch(Exception ex)
            {                
                StringBuilder sb = new StringBuilder($"{ex.Message}");
                sb.AppendLine($"StackTrace: {ex.StackTrace}");
                sb.AppendLine($"InnerException: {ex.InnerException}");

                this.GeneratorErrorCallback(false, 100, sb.ToString(), 1, 1);
                this.ErrorList.BringToFront();
                return null;
            }
        }        

        private string GetXmlInputFile(string inputFileName)
        {            
            // First check, if there isn't file with name "<xslt_file_name>.xml"
            string inputXmlFileName = Path.ChangeExtension(inputFileName, ".xml");
            if(File.Exists(inputXmlFileName))
            {
                return inputXmlFileName;
            }

            // Then check, if there isn't file with name "input.xml"
            string defaultXmlInputFileName = inputXmlFileName.Replace(
                                                                Path.GetFileNameWithoutExtension(inputFileName), 
                                                                "input");
            if(File.Exists(defaultXmlInputFileName))
            {
                return defaultXmlInputFileName;
            }

            // At the end get first xml file in the directory you find
            var allXmlFilesInDirectory = Directory.GetFiles(Path.GetDirectoryName(inputFileName), "*.xml");
            if(null != allXmlFilesInDirectory
                && 0 < allXmlFilesInDirectory.Length)
            {
                return allXmlFilesInDirectory[0];
            }

            return String.Empty;
        }
    }
}
